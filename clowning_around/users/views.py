from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, RedirectView, UpdateView
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from django.shortcuts import get_object_or_404
from .models import Troupe, TroupeLeader, Clown,  Client, Appointment
from .serializers import TroupeSerializer, TroupeLeaderSerializer, ClownSerializer, ClientSerializer, AppointmentSerializer
from rest_framework import status

User = get_user_model()


class UserDetailView(LoginRequiredMixin, DetailView):

    model = User
    slug_field = "username"
    slug_url_kwarg = "username"


user_detail_view = UserDetailView.as_view()


class UserUpdateView(LoginRequiredMixin, UpdateView):

    model = User
    fields = ["name"]

    def get_success_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})

    def get_object(self):
        return User.objects.get(username=self.request.user.username)

    def form_valid(self, form):
        messages.add_message(
            self.request, messages.INFO, _("Infos successfully updated")
        )
        return super().form_valid(form)


user_update_view = UserUpdateView.as_view()


class UserRedirectView(LoginRequiredMixin, RedirectView):

    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})


user_redirect_view = UserRedirectView.as_view()

@api_view(['GET',])
def client_appointments(request, id):
    client = get_object_or_404(Client, id = id)
    appointments = client.appointments.all()
    serialize_appointments = AppointmentSerializer(appointments, context = {'request': request}, many = True)
    return Response({'data': serialize_appointments.data})


@api_view(['POST'])
class rate_appointment(request, id):
    #id = appointment.pk
    #the client will rate the appointment (client_rating)
    appointment = get_object_or_404(Appointment, id = id)
    data = request.data
    serializer = AppointmentSerializer(appointment, data = data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status = 201)
    return Response(serializer.errors, status = 400)

@api_view(['POST',])
class change_appointment_status(request, id):
    appointment = get_object_or_404(Appointment, id = id)
    data = request.data
    serializer = AppointmentSerializer(appointment, data = data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status = 201)
    return Response(serializer.errors, status = 400)

@api_view(['GET',])
class get_client_details(request, id):
     appointment = get_object_or_404(Appointment, id = id)
     client = appointment.client
     client_serializer = ClientSerializer(client, context = {'request': request})
    return Response({data: clientSerializer.data})