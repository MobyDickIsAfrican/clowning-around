from django.urls import path

from clowning_around.users.views import (
    user_redirect_view,
    user_update_view,
    user_detail_view,
    client_appointments,
    rate_appointment,
    change_appointment_status,
    get_client_details
)

app_name = "users"
urlpatterns = [
    path("~redirect/", view=user_redirect_view, name="redirect"),
    path("~update/", view=user_update_view, name="update"),
    path("<str:username>/", view=user_detail_view, name=,"detail"),
    path("api/appointments/client/<int:id>", client_appointments),
    path("api/appointment/<int:id>/rate", rate_appointment),
    path("api/appointment/<int:id>/change-status", change_appointment_status),
    path("api/client/<int:id>/details", get_client_details)  
]
