from rest_framework import serializers
from .models import Troupe, TroupeLeader, Clown,  Client, Appointment

class TroupeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Troupe
        fields = ('pk', 'name', 'max_capacity')

class TroupeLeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = TroupeLeader
        fields = ('pk', 'troupe')

class ClownSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clown
        fields = ('pk', 'user', 'rank', 'troupe')

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('pk', 'user', 'contact_name', 'contact_email', 'contact_number')

class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ('pk', 'troupe_leader', 'client', 'clown', 'status', 'created')